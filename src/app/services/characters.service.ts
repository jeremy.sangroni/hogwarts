import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Character } from '../interfaces/Character';

@Injectable()
export class CharactersService {

  constructor(private http:HttpClient) { }

  getAllCharacters():Observable<Character[]>{
    return this.http.get<Character[]>('http://hp-api.herokuapp.com/api/characters');
  }

  getCharacterByHouse(house:String):Observable<Character[]>{
    return this.http.get<Character[]>(`http://hp-api.herokuapp.com/api/characters/house/${house}`);
  }

}
