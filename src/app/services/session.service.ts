import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Student } from '../interfaces/Student';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private studensKey = "students";

  constructor(private _snackBar: MatSnackBar) { }

  saveStudent(student:Student):Observable<Student>{
    try {
      let students = this.getStudents() || [];
      students.push(student);
      sessionStorage.setItem(this.studensKey,JSON.stringify(students))
    } catch (error) {
      this._snackBar.open('An error occurred saving the student','Ok',{duration:5000});
    }
    return of(student);
  }

  createListAddNewStudent(){

  }

  getStudents():Student[]{
    let students:string | null = sessionStorage.getItem(this.studensKey);
    return JSON.parse(students || '[]');
  }
}
