import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Teacher } from '../interfaces/Teacher';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  constructor(private http:HttpClient) { }

  getAllTeachers():Observable<Teacher[]>{
    return this.http.get<Teacher[]>('http://hp-api.herokuapp.com/api/characters/staff');
  }
}
