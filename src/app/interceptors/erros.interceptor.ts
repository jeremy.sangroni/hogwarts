import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrosInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
              console.log('Event: ', event);
          }
          return event;
      }),
      catchError((error: HttpErrorResponse) => {
        switch ((<HttpErrorResponse>error).status) {
          case 401:
              return this.handle401Error(error);
          case 403:
              return this.handle403Error(error);
          case 404:
            return this.handle404Error(error);
          case 500:
            return this.handle500Error(error);
          default:
            break;
        }
          return throwError(error);
      }));
  }

  //Logica para el manejo de error HTTP 401 - Not Authorized
  private handle401Error(error: HttpErrorResponse){
    return throwError(error);
  }
  //Logica para el manejo de error HTTP 403 - Forbidden
  private handle403Error(error: HttpErrorResponse){
    return throwError(error);
  }
  //Logica para el manejo de error HTTP 404 - Not Found
  private handle404Error(error: HttpErrorResponse){
    return throwError(error);
  }
  //Logica para el manejo de error HTTP 500 - Internal server error
  private handle500Error(error: HttpErrorResponse){
    return throwError(error);
  }
}
