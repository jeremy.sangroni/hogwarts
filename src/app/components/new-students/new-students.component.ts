import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { Student } from 'src/app/interfaces/Student';
import { GeneralInfoComponent } from '../general-info/general-info.component';
import { GeneralInfo } from 'src/app/interfaces/GeneralInfo';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-new-students',
  templateUrl: './new-students.component.html',
  styleUrls: ['./new-students.component.css']
})
export class NewStudentsComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'patronus', 'dateofbirth'];
  public dataSource = new MatTableDataSource<Student>([]);
  public house:FormControl = new FormControl();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private session:SessionService,
    public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(){
    this.getNewStudents();
  }

  private getNewStudents(){
    let students = this.session.getStudents();
    this.setDataSource(students);
  }

  private setDataSource(data:Student[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public getDetailImage(info:GeneralInfo){
    this.dialog.open(GeneralInfoComponent,{
      data:info
    });
  }


}
