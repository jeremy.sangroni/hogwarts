import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GeneralInfo } from 'src/app/interfaces/GeneralInfo';

@Component({
  selector: 'app-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.css']
})
export class GeneralInfoComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: GeneralInfo) { }

  ngOnInit(): void {}

}
