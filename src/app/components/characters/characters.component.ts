import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { GeneralInfo } from 'src/app/interfaces/GeneralInfo';
import { CharactersService } from 'src/app/services/characters.service';
import { Character } from '../../interfaces/Character';
import { GeneralInfoComponent } from '../general-info/general-info.component';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements AfterViewInit  {
  public displayedColumns: string[] = ['name', 'patronus', 'dateofbirth', 'image'];
  public dataSource = new MatTableDataSource<Character>([]);
  public house:FormControl = new FormControl();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private charactersService:CharactersService,
    public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(){
    this.getCharacters();
  }

  private getCharacters(){
    this.charactersService.getAllCharacters()
    .subscribe((data:Character[])=>{
      this.setDataSource(data);
    });
  }

  private setDataSource(data:Character[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  public changeHouse(value:String){
    value === 'all' ? this.getCharacters() : this.getCharacterByHouse(value);

  }

  private getCharacterByHouse(value:String){
    this.charactersService.getCharacterByHouse(value)
    .subscribe((data:Character[])=>{
      this.setDataSource(data);
    });
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public getDetailImage(info:GeneralInfo){
    this.dialog.open(GeneralInfoComponent,{
      data:info
    });
  }

}
