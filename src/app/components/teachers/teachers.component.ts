import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { GeneralInfo } from 'src/app/interfaces/GeneralInfo';
import { Teacher } from 'src/app/interfaces/Teacher';
import { TeachersService } from 'src/app/services/teachers.service';
import { GeneralInfoComponent } from '../general-info/general-info.component';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {
  displayedColumns: string[] = ['name', 'patronus', 'dateofbirth', 'image'];
  dataSource = new MatTableDataSource<Teacher>([]);
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  public house:FormControl = new FormControl();

  constructor(private teachersService:TeachersService,
    public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(){
    this.getTeachers();
  }

  getTeachers(){
    this.teachersService.getAllTeachers()
    .subscribe((data:Teacher[])=>{
      this.setDataSource(data);
    });
  }

  setDataSource(data:Teacher[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public getDetailImage(info:GeneralInfo){
    this.dialog.open(GeneralInfoComponent,{
      data:info
    });
  }

}
