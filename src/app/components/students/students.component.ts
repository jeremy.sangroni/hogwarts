import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { StudentsService } from 'src/app/services/students.service';
import {MatDialog} from '@angular/material/dialog';
import { Student } from 'src/app/interfaces/Student';
import { NewStudentComponent } from './components/new-student/new-student.component';
import { GeneralInfoComponent } from '../general-info/general-info.component';
import { GeneralInfo } from 'src/app/interfaces/GeneralInfo';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'patronus', 'dateofbirth', 'image'];
  public dataSource = new MatTableDataSource<Student>([]);
  public house:FormControl = new FormControl();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private studentsService:StudentsService,
    public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(){
    this.getStudents();
  }

  private getStudents(){
    this.studentsService.getAllStudents()
    .subscribe((data:Student[])=>{
      this.setDataSource(data);
    });
  }

  private setDataSource(data:Student[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public newStudent(){
    const dialogRef = this.dialog.open(NewStudentComponent);
  }

  public getDetailImage(info:GeneralInfo){
    this.dialog.open(GeneralInfoComponent,{
      data:info
    });
  }

}
