import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-new-student',
  templateUrl: './new-student.component.html',
  styleUrls: ['./new-student.component.css']
})
export class NewStudentComponent implements OnInit {

  public formGroup!:FormGroup;

  constructor(private fb:FormBuilder,
    private sessionService:SessionService,
    private datePipe : DatePipe,
    private _snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.setForm();
  }

  setForm(){
    this.formGroup = this.fb.group({
      name:['',Validators.required],
      patronus:['',Validators.required],
      dateofbirth:['',Validators.required],
      image:['',Validators.required],
    });
  }

  save(){
    this.sessionService.saveStudent(this.getContract()).subscribe(data=> {
      this._snackBar.open('Student saved successfully','Ok',{duration:5000});
    });
  }

  getContract(){
    let contract = this.formGroup.value;
    contract.image = this.formGroup.value.image.name;
    contract.dateofbirth = this.datePipe.transform(this.formGroup.value.dateofbirth,'MM-dd-yyyy');
    return contract;
  }

}
