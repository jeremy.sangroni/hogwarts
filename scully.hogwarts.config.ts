import { ScullyConfig } from '@scullyio/scully';
export const config: ScullyConfig = {
  projectRoot: "./src",
  projectName: "hogwarts",
  outDir: './dist/static',
  routes: {
  }
};